# Ecosystem Map

## Description
This project aims to provide a 3D interactive visual for the Catalyst ecosystem.
It uses a Neo4j graph database to hold the data structures.
Querying the graph to extract data for rendering is done using the Cypher language.
The public URL for the [latest release is available here.](https://swarmlab2.gitlab.io/ecosystem-map/)

 - https://swarmlab2.gitlab.io/ecosystem-map/

The Project proposal seeking to find this work including the roadmap is [available on Cardano Ideascale](https://cardano.ideascale.com/c/idea/422999)

## Importing data
Community data is collected via Google Forms.
CSVs are exported and then imported into Neo4j using [CSV import](https://neo4j.com/docs/aura/auradb/importing/importing-data/#_neo4j_data_importer).

## Reference material
The [original inspiration for this code](https://neo4j.com/developer-blog/visualizing-graphs-in-3d-with-webgl/) solution came from Neo4j's developer blog.

Other references:

<details><summary>Click to expand</summary>
[Introduction to Neo4j](https://www.youtube.com/watch?v=lLZWVwMA5aM)
[Cypher Query Language](https://neo4j.com/developer/cypher/)
[Cypher Quick Reference Card](https://neo4j.com/docs/cypher-refcard/current/)

![Cypher Query Structure 1](https://dist.neo4j.com/wp-content/uploads/sample-cypher.png)
![Cypher Query Structure 2](https://swarmlab2.gitlab.io/ecosystem-map/images/cypher-structure.png)
</details>


## Installation
The HTML and JS for the site are hosted from Gitlabs Pages.
The Neo4j instance is accessed here.
[Login](https://console.neo4j.io/?ref=aura-lp&mpp=4bfb2414ab973c741b6f067bf06d5575&mpid=auth0%7C620b015cd309d0006a900d48&_gl=1*upp7f2*_ga*MTIyMTc3OTc4OS4xNjU2MjYyNjM1*_ga_DL38Q8KGQC*MTY1ODIzMjEwNS4yMi4xLjE2NTgyMzQwNTcuMA..&_ga=2.1153137.195705627.1658144574-1221779789.1656262635)
Credentials can be acquired from the Gitlab Owner

## Support
For questions and help, please [contact Fluid7](https://pool.fluid7.com/contact-us/)
or reach out on the Catalyst Swarm Discord server in the [⬜-ecosystem-map](https://discord.com/channels/832894680290951179/988200786523750480) channel

## Authors and acknowledgment
 - The Ecosystem Map was spawned from the mind of [Felix\[SWARM\]](https://pool.pm/e40edb5a243c9ef00296860c6b7c8272fd5923ac52b5050d68e80d9b)
 - Architecture and implentation was managed by [Jonathan\[FLUID\]](https://pool.pm/38c2ea35c92392314b84f70535632b1b8ac3d3bd99fa7786296b160c)
 - The original Proof of Concept was built by [Giovanni\[EASY1\]](https://pool.pm/20df8645abddf09403ba2656cda7da2cd163973a5e439c6e43dcbea9)

Please delegate to any of these pools to support our efforts.



***

## Collaborate with the team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
