package com.github.speedwing.model;

import java.util.Objects;
import java.util.Optional;

public class Entity {

    private final String name;

    private final Optional<String> telegramChannel;

    public Entity(String name, Optional<String> telegramChannel) {
        this.name = name;
        this.telegramChannel = telegramChannel;
    }

    public String getName() {
        return name;
    }

    public Optional<String> getTelegramChannel() {
        return telegramChannel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return name.equals(entity.name) && telegramChannel.equals(entity.telegramChannel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, telegramChannel);
    }

}
