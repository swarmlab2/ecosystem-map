package com.github.speedwing.services;

import com.github.speedwing.Person;
import org.neo4j.driver.*;
import org.neo4j.driver.exceptions.Neo4jException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class Neo4jService implements AutoCloseable {
    private static final Logger LOGGER = Logger.getLogger(Neo4jService.class.getName());
    private final Driver driver;

    public Neo4jService() {
        String uri = "neo4j+s://9e0eb8db.databases.neo4j.io";

        String user = "neo4j";
        String password = "zMW9CzeHyy1GqgbCR9M57wmikBf-OYtRAE-dTqHQBw0";

        // The driver is a long living object and should be opened during the start of your application
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password), Config.defaultConfig());
    }

    @Override
    public void close() throws Exception {
        // The driver object should be closed before the application ends.
        driver.close();
    }

    public void dropAll() {
        String query = "MATCH (n) DETACH DELETE (n)";
        Session session = driver.session();
        session.writeTransaction(tx -> {
            tx.run(query);
            return null;
        });
    }

    public void createEntity(final String entityName, final List<String> tags, final Integer createdAt, final Integer weight) {
        String query = "MERGE (e: Entity { name: $entity_name, tags: $tags, created_at: $createdAt, weight: $weight }) RETURN e";
        Map<String, Object> params = new HashMap<>();
        params.put("entity_name", entityName.trim());
        params.put("tags", tags);
        params.put("createdAt", createdAt);
        params.put("weight", weight);
        Session session = driver.session();
        session.writeTransaction(tx -> {
            Result result = tx.run(query, params);
            return result.single();
        });
    }

    public void createEntityRelation(final String firstEntity, final String secondEntity) {
        String query = "MATCH " +
                "(e1: Entity { name: $firstEntity }), " +
                "(e2: Entity { name: $secondEntity }) " +
                "MERGE (e1)-[c:COLLAB]-(e2) " +
                "RETURN type(c)";
        Map<String, Object> params = new HashMap<>();
        params.put("firstEntity", firstEntity);
        params.put("secondEntity", secondEntity);
        Session session = driver.session();
        session.writeTransaction(tx -> {
            Result result = tx.run(query, params);
            return result.single();
        });
    }

    public void createFriendship(final String person1Name, final String person2Name) {
        // To learn more about the Cypher syntax, see https://neo4j.com/docs/cypher-manual/current/
        // The Reference Card is also a good resource for keywords https://neo4j.com/docs/cypher-refcard/current/
        String createFriendshipQuery = "CREATE (p1:Person { name: $person1_name })\n" +
                "CREATE (p2:Person { name: $person2_name })\n" +
                "CREATE (p1)-[:KNOWS]->(p2)\n" +
                "RETURN p1, p2";

        Map<String, Object> params = new HashMap<>();
        params.put("person1_name", person1Name);
        params.put("person2_name", person2Name);

        try (Session session = driver.session()) {
            // Write transactions allow the driver to handle retries and transient errors
            Record record = session.writeTransaction(tx -> {
                Result result = tx.run(createFriendshipQuery, params);
                return result.single();
            });
            System.out.println(String.format("Created friendship between: %s, %s",
                    record.get("p1").get("name").asString(),
                    record.get("p2").get("name").asString()));
            // You should capture any errors along with the query and data for traceability
        } catch (Neo4jException ex) {
            LOGGER.log(Level.SEVERE, createFriendshipQuery + " raised an exception", ex);
            throw ex;
        }
    }

    public String findPerson(final String personName) {
        String readPersonByNameQuery = "MATCH (p:Person)\n" +
                "WHERE p.name = $person_name\n" +
                "RETURN p.name AS name";

        Map<String, Object> params = Collections.singletonMap("person_name", personName);

        try (Session session = driver.session()) {
            Record record = session.readTransaction(tx -> {
                Result result = tx.run(readPersonByNameQuery, params);
                return result.single();
            });
            return String.format("Found person: %s", record.get("name").asString());
            // You should capture any errors along with the query and data for traceability
        } catch (Neo4jException ex) {
            LOGGER.log(Level.SEVERE, readPersonByNameQuery + " raised an exception", ex);
            throw ex;
        }
    }

    public Person findActualPerson(final String personName) {
        String readPersonByNameQuery = "MATCH (p:Person)\n" +
                "WHERE p.name = $person_name\n" +
                "RETURN p";

        Map<String, Object> params = Collections.singletonMap("person_name", personName);

        try (Session session = driver.session()) {
            Record record = session.readTransaction(tx -> {
                Result result = tx.run(readPersonByNameQuery, params);
                return result.single();
            });

            String name = record.get("p").get("name").asString();
            Integer born = record.get("p").get("born").asInt();

            return new Person(name, born);
            // You should capture any errors along with the query and data for traceability
        } catch (Neo4jException ex) {
            LOGGER.log(Level.SEVERE, readPersonByNameQuery + " raised an exception", ex);
            throw ex;
        }
    }

    public void findMoviePerson(final String personName) {
        String readPersonByNameQuery = "MATCH (p:Person)\n" +
                "WHERE p.name = $person_name\n" +
                "RETURN p.name AS name";

        Map<String, Object> params = Collections.singletonMap("person_name", personName);

        try (Session session = driver.session()) {
            Record record = session.readTransaction(tx -> {
                Result result = tx.run(readPersonByNameQuery, params);
                return result.single();
            });
            System.out.println(String.format("Found person: %s", record.get("name").asString()));
            // You should capture any errors along with the query and data for traceability
        } catch (Neo4jException ex) {
            LOGGER.log(Level.SEVERE, readPersonByNameQuery + " raised an exception", ex);
            throw ex;
        }
    }

}