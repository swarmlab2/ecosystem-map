package com.github.speedwing;

import java.util.Objects;

public class Person {

    private String name;

    private Integer born;

    public String getName() {
        return name;
    }

    public Integer getBorn() {
        return born;
    }

    public Person(String name, Integer born) {
        this.name = name;
        this.born = born;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) && born.equals(person.born);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, born);
    }
}
