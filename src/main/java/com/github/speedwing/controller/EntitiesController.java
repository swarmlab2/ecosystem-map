package com.github.speedwing.controller;

import com.github.speedwing.Person;
import com.github.speedwing.services.Neo4jService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class EntitiesController {

    Logger logger = LoggerFactory.getLogger(EntitiesController.class);

//    https://neo4j.com/docs/cypher-manual/current/clauses/merge/#query-merge-using-unique-constraints
//    CREATE CONSTRAINT FOR (n:Person) REQUIRE n.name IS UNIQUE;
//CREATE CONSTRAINT FOR (n:Person) REQUIRE n.role IS UNIQUE;


    //    create constraint for (e:Entity) require e.name is unique;
    private Neo4jService neo4JService;

    public EntitiesController(Neo4jService neo4JService) {
        this.neo4JService = neo4JService;
    }

    @PostMapping("/entity")
    public void createEntity(@RequestParam String name, @RequestParam(required = false) Optional<String> telegramChannel) {
        System.out.println("name: " + name);
        System.out.println("telegramChannel: " + telegramChannel);
//        neo4jDriver
    }

    private List<String> getOrganizations() {
        InputStream resourceAsStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("organizations.csv");

        BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));
        return reader
                .lines()
                .collect(Collectors.toList());
    }

    private List<String> getRyuukiRelationships() {
        InputStream resourceAsStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("ryuuki.csv");

        BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));
        return reader
                .lines()
                .collect(Collectors.toList());
    }

    @GetMapping("/entity/init")
    public void init() {

        logger.info("hello world");

        // Drop all
        neo4JService.dropAll();

        List<String> organizations = getOrganizations();

        organizations
                .forEach(line -> {
                    String[] parts = line.split(",");
                    String org = parts[0];
                    String[] tags = parts[1].split("\\|");
                    Integer weight = Integer.parseInt(parts[2]);
                    Integer createdAt = Integer.parseInt(parts[3]);
                    neo4JService.createEntity(org, Arrays.asList(tags), createdAt, weight);
                });

        List<String> ryuukiRelationships = getRyuukiRelationships();
        ryuukiRelationships
                .forEach(line -> {
                    String[] split = line.split(",");
                    List<String> entities = Arrays.stream(split).map(String::trim).collect(Collectors.toList());
                    if (entities.size() > 0) {
                        String firstEntity = entities.get(0);
                        entities
                                .stream()
                                .skip(1L)
                                .forEach(otherEntity -> neo4JService.createEntityRelation(firstEntity, otherEntity));
                    }
                });

    }

    @GetMapping("/person")
    public String person() {
        return neo4JService.findPerson("Tom Hanks");
    }

    @GetMapping("/actual_person")
    public Person actualPerson() {
        return neo4JService.findActualPerson("Tom Hanks");
    }

}
