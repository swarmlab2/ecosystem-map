package com.github.speedwing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackages = "com.github.speedwing")
public class Main {


    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }


}
