const driver = neo4j.driver("neo4j+s://9e0eb8db.databases.neo4j.io", neo4j.auth.basic("neo4j", "zMW9CzeHyy1GqgbCR9M57wmikBf-OYtRAE-dTqHQBw0"));

const Graph = ForceGraph3D({
  extraRenderers: [new THREE.CSS2DRenderer()]
})
(document.getElementById('3d-graph'))

let toggleData;
(toggleData = function() {

    const year = parseInt(document.getElementById('select').value);
    console.log(year);

    const tag = document.getElementById('tag').value;
    const tagFlag = tag == 'all';
    console.log(tag);
    console.log(tagFlag);

	Graph.resetProps(); // Wipe current state
//	dataSet(Graph); // Load data set

    const session = driver.session();

    const condition = tagFlag ? '' : 'and any(tag IN n.tags WHERE tag = $tag) and any(tag IN m.tags WHERE tag = $tag) '
    console.log(condition);

    session
      .run('MATCH (n)-[r]->(m) ' +
      ' WHERE n.created_at <= $year and m.created_at <= $year ' +
      condition +
        'RETURN { id: id(n), label:head(labels(n)), caption:n.name, size:n.weight } as source, ' +
       '{ id: id(m), label:head(labels(m)), caption:m.name, size:m.weight } as target, {weight:log(r.weight), type:type(r)} as rel ' +
       'LIMIT $limit', {limit: neo4j.int(5000), year: year, tag: tag})
      .then(function (result) {
        const nodes = {}
        const links = result.records.map(r => {
	       var source = r.get('source');
	       source.id = source.id.toNumber();
           nodes[source.id] = source;
	       var target = r.get('target');
	       target.id = target.id.toNumber();
           nodes[target.id] = target;
           var rel = r.get('rel');
	       return Object.assign({source:source.id,target:target.id}, rel);
	    });
        session.close();

        const gData = { nodes: Object.values(nodes), links: links}


          Graph.graphData(gData)
          .nodeAutoColorBy('size')
          .nodeVal('size')
          .linkWidth('weight')
          .nodeThreeObject(node => {
            const nodeEl = document.createElement('div');
            nodeEl.textContent = node.caption;
            nodeEl.style.color = node.color;
            nodeEl.className = 'node-label';
            return new THREE.CSS2DObject(nodeEl);
          })
          .nodeThreeObjectExtend(true)
        ;

      })
      .catch(function (error) {
        console.log(error);
      });


})(); // IIFE init

